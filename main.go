package main

import (
	"github.com/urfave/cli/v2"
	"os"
)

func main() {
	app := &cli.App{
		Name:  "semgrep",
		Usage: "Semgrep analyzer",
		Commands: []*cli.Command{
			analyzeCommand(),
		},
	}

	if err := app.Run(os.Args); err != nil {
		logger.Fatal(err)
	}
}
