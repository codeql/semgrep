package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-resty/resty/v2"
	"io/ioutil"
)

type RuleResponse struct {
	Rules []interface{} `json:"rules"`
}

func GetRules(url, token string) {
	var result RuleResponse
	_, err := resty.New().R().
		SetHeader("Authorization", fmt.Sprintf("Bearer %s", token)).
		SetResult(&result).
		SetHeader("Accept", "application/json").
		Get(url)
	if err != nil {
		fmt.Errorf(err.Error())
	}
	fmt.Println(len(result.Rules))
	data, _ := json.Marshal(result)
	ioutil.WriteFile("rules/default.json", data, 777)
}
