package main

import (
	"encoding/json"
	"github.com/urfave/cli/v2"
	"semgrep/report"
	"semgrep/safebug"
	"semgrep/sarif"
	"semgrep/types"
)

const (
	flagExclude      = "exclude"
	flagProMode      = "pro"
	flagConfigs      = "config"
	flagSeverities   = "severity"
	flagVerbose      = "verbose"
	flagSafebugUrl   = "safebug-url"
	flagSafebugToken = "safebug-token"
	flagSarifOutput  = "sarif"
	AnalyzeCommand   = "run"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.BoolFlag{
			Name:    flagProMode,
			Usage:   "Inter-file analysis and Pro languages. Requires Semgrep Pro Engine",
			EnvVars: []string{"SEMGREP_PRO"},
			Value:   true,
		},
		&cli.StringFlag{
			Name:    flagExclude,
			Usage:   "Skip any file or directory that matches this pattern",
			EnvVars: []string{"SEMGREP_EXCLUDED_PATHS"},
			Value:   "",
		},
		&cli.StringFlag{
			Name:    flagSeverities,
			Usage:   "Skip any file or directory that matches this pattern",
			EnvVars: []string{"SEMGREP_SEVERITY"},
			Value:   "",
		},
		&cli.StringFlag{
			Name:    flagConfigs,
			Usage:   "YAML configuration file, directory of YAML files ending in .yml|.yaml, URL of a configuration file, or Semgrep registry entry name.",
			EnvVars: []string{"SEMGREP_CONFIG"},
			Value:   "/rules/default.json",
		},
		&cli.BoolFlag{
			Name:    flagVerbose,
			Usage:   "Show verbose",
			EnvVars: []string{"SEMGREP_DEBUG", "SEMGREP_VERBOSE"},
			Value:   false,
		},
		&cli.StringFlag{
			Name:    flagSafebugUrl,
			Usage:   "SafeBug URL",
			EnvVars: []string{"SAFEBUG_URL"},
		},
		&cli.StringFlag{
			Name:    flagSafebugToken,
			Usage:   "SafeBug Token",
			EnvVars: []string{"SAFEBUG_TOKEN"},
		},
		&cli.BoolFlag{
			Name:    flagSarifOutput,
			Usage:   "Sarif output",
			Value:   false,
			EnvVars: []string{"SEMGREP_SARIF"},
		},
	}
}

func analyzeCommand() *cli.Command {
	flags := analyzeFlags()
	return &cli.Command{
		Name:  AnalyzeCommand,
		Usage: "Scan SAST",
		Flags: flags,
		Action: func(context *cli.Context) error {
			analyzer := Analyzer{
				Configs:       context.String(flagConfigs),
				Severities:    context.String(flagSeverities),
				ProEngine:     context.Bool(flagProMode),
				ExcludedPaths: context.String(flagExclude),
				Verbose:       context.Bool(flagVerbose),
				safebug:       safebug.NewSafeBug(context.String(flagSafebugUrl), context.String(flagSafebugToken)),
				GitlabCI:      NewEnvGitlabCI(),
				SarifOutput:   context.Bool(flagSarifOutput),
			}
			//check connect to safebug
			analyzer.safebug.RefreshToken()
			data, err := analyzer.Scan(".")
			if err != nil {
				logger.Fatal(err.Error())
			}
			var vulnerabilities []types.Vulnerability
			if analyzer.SarifOutput {
				var rep sarif.Sarif
				err = json.Unmarshal(data, &rep)
				if err != nil {
					logger.Error(err.Error())
				}
				vulnerabilities = sarif.SarifToVulnerabilities(&rep, analyzer.GitlabCI.ProjectURL)
			} else {
				var rep report.Report
				err = json.Unmarshal(data, &rep)
				if err != nil {
					logger.Error(err.Error())
				}
				vulnerabilities = report.ConvertReportToVulnerabilities(rep, analyzer.GitlabCI.ProjectURL)
			}
			analyzer.Analyze(vulnerabilities)
			return nil
		},
	}
}
