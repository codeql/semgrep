package sarif

import (
	"regexp"
	"semgrep/safebug"
	"strings"
)

// match CWE-XXX only
var cweIDRegex = regexp.MustCompile(`([cC][wW][eE])-(\d{1,4})`)

// match (TYPE)-(ID): (Description)
var tagIDRegex = regexp.MustCompile(`([^-]+)-([^:]+):\s*(.+)`)

type Rule struct {
	ID               string `json:"id"`
	Name             string `json:"name"`
	ShortDescription struct {
		Text string `json:"text"`
	} `json:"shortDescription"`
	FullDescription struct {
		Text string `json:"text"`
	} `json:"fullDescription"`
	DefaultConfiguration struct {
		Level string `json:"level"`
	} `json:"defaultConfiguration"`
	Properties ruleProperties `json:"properties"`
	HelpURI    string         `json:"helpUri"`
	Help       struct {
		Markdown string `json:"markdown"`
		Text     string `json:"text"`
	} `json:"help"`
}

type ruleProperties struct {
	Precision        string   `json:"precision"`
	Tags             []string `json:"tags"`
	SecuritySeverity string   `json:"security-severity"`
}

func findTagMatches(tag string) []string {
	// first try to extract (TAG)-(ID): (Description)
	matches := tagIDRegex.FindStringSubmatch(tag)

	if matches == nil {
		// see if we just have a CWE-(XXX) tag
		matches = cweIDRegex.FindStringSubmatch(tag)
	}
	return matches
}

func (r *Rule) Message() string {
	// prefer shortDescription field over tag: CWE: <text> for semgrep
	if r.ShortDescription.Text != "" && !strings.EqualFold(r.FullDescription.Text, r.ShortDescription.Text) {
		return r.ShortDescription.Text
	}

	for _, tag := range r.Properties.Tags {
		splits := strings.Split(tag, ":")
		if strings.HasPrefix(splits[0], "CWE") && len(splits) > 1 {
			return strings.TrimLeft(splits[1], " ")
		}
	}
	// default.json to full text description
	return r.FullDescription.Text
}

func (r *Rule) Severity() string {
	switch r.DefaultConfiguration.Level {
	case "error":
		return safebug.Critical
	case "warning":
		return safebug.Medium
	default:
		return safebug.Info
	}
}
