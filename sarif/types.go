package sarif

import (
	"fmt"
	"os"
	"strings"
)

type Sarif struct {
	Schema  string `json:"$schema"`
	Version string `json:"version"`
	Runs    []Run  `json:"runs"`
}

type Run struct {
	Invocations []invocation `json:"invocations"`
	Results     []Result     `json:"results"`
	Tool        struct {
		Driver struct {
			Name            string `json:"name"`
			SemanticVersion string `json:"semanticVersion"`
			Rules           []Rule `json:"rules"`
		} `json:"driver"`
	} `json:"tool"`
}

type invocation struct {
	ExecutionSuccessful        bool           `json:"executionSuccessful"`
	ToolExecutionNotifications []notification `json:"toolExecutionNotifications"`
}

type notification struct {
	Descriptor struct {
		ID string `json:"id"`
	} `json:"descriptor"`
	Level   string `json:"level"`
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
}

type Result struct {
	RuleID  string `json:"ruleId"`
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
	Locations []struct {
		PhysicalLocation physicalLocation `json:"physicalLocation"`
	} `json:"locations"`
	Fingerprints struct {
		Id string `json:"matchBasedId/v1"`
	} `json:"fingerprints"`
	CodeFlows    []codeFlow `json:"codeFlows"`
	Suppressions []struct {
		Kind   string `json:"kind"`             // values= 'inSource', 'external'
		Status string `json:"status,omitempty"` // values= empty,'accepted','underReview','rejected'
		GUID   string `json:"guid,omitempty"`
	} `json:"suppressions,omitempty"`
}

func realUri(uri, message string) string {
	if strings.Contains(message, uri) {
		return uri
	}
	if strings.Contains(message, "@") {
		s := strings.Split(message, "@")[1]
		s = strings.TrimSpace(strings.Split(s, ":")[0])
		s = strings.ReplaceAll(s, "'", "")
		return s
	}
	return uri
}
func (this *Result) Flow() string {
	s := ""
	baseUri := fmt.Sprintf("%s/-/blob/%s", os.Getenv("CI_PROJECT_URL"), os.Getenv("CI_COMMIT_SHA"))
	for _, codeFlow := range this.CodeFlows {
		for _, threadFlow := range codeFlow.ThreadFlows {
			if len(threadFlow.Locations) > 0 {
				location := threadFlow.Locations[0].Location
				uri := fmt.Sprintf("%s/%s#L%d", baseUri, realUri(location.PhysicalLocation.ArtifactLocation.Uri, location.Message.Text), location.PhysicalLocation.Region.StartLine)
				s += fmt.Sprintf("**Taint comes from**\n - [%s](%s)\n\n", location.Message.Text, uri)
			}
			if len(threadFlow.Locations) > 2 {
				s += fmt.Sprintf("**Taint flows through these intermediate variables**\n")
				for i := 1; i < len(threadFlow.Locations)-1; i++ {
					location := threadFlow.Locations[i].Location
					uri := fmt.Sprintf("%s/%s#L%d", baseUri, realUri(location.PhysicalLocation.ArtifactLocation.Uri, location.Message.Text), location.PhysicalLocation.Region.StartLine)
					s += fmt.Sprintf("- [%s](%s)\n", location.Message.Text, uri)
				}
				s += "\n"
			}
			if len(threadFlow.Locations) > 1 {
				location := threadFlow.Locations[len(threadFlow.Locations)-1].Location
				uri := fmt.Sprintf("%s/%s#L%d", baseUri, location.PhysicalLocation.ArtifactLocation.Uri, location.PhysicalLocation.Region.StartLine)
				s += fmt.Sprintf("**This is how taint reaches the sink**\n- [%s](%s)\n\n", location.Message.Text, uri)
			}
			s += "_______________________\n"
		}
	}
	return s
}

type physicalLocation struct {
	ArtifactLocation struct {
		Uri string `json:"uri"`
	} `json:"artifactLocation"`
	Region struct {
		EndColumn int `json:"endColumn"`
		EndLine   int `json:"endLine"`
		Message   struct {
			Text string `json:"text"`
		} `json:"message"`
		Snippet struct {
			Text string `json:"text"`
		} `json:"snippet"`
		StartColumn int `json:"startColumn"`
		StartLine   int `json:"startLine"`
	} `json:"region"`
}

type codeFlow struct {
	Message struct {
		Text string `json:"text"`
	} `json:"message"`
	ThreadFlows []struct {
		Locations []struct {
			Location struct {
				Message struct {
					Text string `json:"text"`
				} `json:"message"`
				PhysicalLocation physicalLocation `json:"physicalLocation"`
			} `json:"location"`
			NestingLevel int `json:"nestingLevel"`
		} `json:"locations"`
	} `json:"threadFlows"`
}
