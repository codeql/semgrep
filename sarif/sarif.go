package sarif

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"semgrep/types"
)

const MaxLengthMessage = 1048576

func SarifToVulnerabilities(report *Sarif, rootPath string) []types.Vulnerability {
	var vulnerabilities []types.Vulnerability
	mExists := make(map[string]bool)
	for _, run := range report.Runs {
		issues, err := transformRun(run, rootPath)
		if err != nil {
			logrus.Error(err.Error())
		}
		for _, issue := range issues {
			if _, exist := mExists[issue.Identity]; exist == false {
				vulnerabilities = append(vulnerabilities, issue)
				mExists[issue.Identity] = true
			}
		}
	}
	return vulnerabilities
}

func transformRun(r Run, rootPath string) ([]types.Vulnerability, error) {
	ruleMap := make(map[string]Rule)
	for _, rule := range r.Tool.Driver.Rules {
		ruleMap[rule.ID] = rule
	}

	var issues []types.Vulnerability
	for _, result := range r.Results {
		rule := ruleMap[result.RuleID]
		var description string
		if len(result.Message.Text) > MaxLengthMessage {
			description = result.Message.Text[:MaxLengthMessage]
		} else {
			description = result.Message.Text
			codeFlow := result.Flow()
			if codeFlow != "" {
				description += fmt.Sprintf("\n\n**`Vulnerability Flow`**\n\n%s", codeFlow)
			}
		}
		for _, location := range result.Locations {
			lineEnd := location.PhysicalLocation.Region.EndLine
			lineStart := location.PhysicalLocation.Region.StartLine
			uri := fmt.Sprintf("%s/-/blob/%s/%s#L%d-L%d", rootPath, os.Getenv("CI_COMMIT_SHA"), location.PhysicalLocation.ArtifactLocation.Uri, lineStart, lineEnd)
			issue := types.Vulnerability{
				ID:          "",
				Name:        rule.Message(),
				Description: description,
				Category:    "Other",
				Severity:    rule.Severity(),
				Location:    uri,
				Identity:    result.Fingerprints.Id,
			}
			issues = append(issues, issue)
			break
		}
	}
	return issues, nil
}
