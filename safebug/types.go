package safebug

type AccessToken struct {
	Token string `json:"token"`
}

type CreateUpdateProjectGitlabRequest struct {
	Name             string `json:"name"`
	ProjectGitlabId  string `json:"projectGitlabId"`
	ProjectGitlabUrl string `json:"projectGitlabUrl"`
}

type CreateSastIssueRequest struct {
	Name        string `json:"name"`
	Category    string `json:"category"`
	Description string `json:"description"`
	Severity    string `json:"severity"`
	Solution    string `json:"solution"`
	Reproduce   string `json:"reproduce"`
	Endpoint    string `json:"endpoint"`
	Identity    string `json:"identity"`
}

type CommentIssue struct {
	Comment  *string `json:"comment"`
	Status   *string `json:"status"`
	Severity *string `json:"severity"`
}

type SastReportSummary struct {
	Branch      string `json:"branch"`
	PipelineId  string `json:"pipelineId"`
	PipelineUrl string `json:"pipelineUrl"`
	QualityGate bool   `json:"qualityGate"`
	NewIssue    int    `json:"newIssue"`
	FixedIssue  int    `json:"fixedIssue"`
}

type SastIssueSummary struct {
	ID       string `json:"id"`
	Status   string `json:"status"`
	Severity string `json:"severity"`
	Identity string `json:"identity"`
	Name     string `json:"name"`
}

type SastThresholdConfig struct {
	Enable   bool `json:"enable"`
	Critical int  `json:"critical"`
	High     int  `json:"high"`
	Medium   int  `json:"medium"`
	Low      int  `json:"low"`
}
