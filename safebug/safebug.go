package safebug

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"net/url"
)

type SafeBug struct {
	Token       string
	AccessToken string
	BaseURL     string
	client      *resty.Client
}

func NewSafeBug(baseUrl string, token string) *SafeBug {
	return &SafeBug{
		Token:   token,
		BaseURL: baseUrl,
		client:  resty.New(),
	}
}

func (safebug *SafeBug) RefreshToken() {
	api, _ := url.JoinPath(safebug.BaseURL, "api/refresh-token")
	var token AccessToken
	resp, err := safebug.client.R().
		SetBody(map[string]string{"token": safebug.Token}).
		SetResult(&token).
		Post(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() != 200 {
		logrus.Panic("Unauthorized: SAFEBUG_TOKEN invalid")
	}
	safebug.AccessToken = token.Token
}

func (safebug *SafeBug) CreateOrUpdateProjectGitlab(body CreateUpdateProjectGitlabRequest) string {
	api, _ := url.JoinPath(safebug.BaseURL, "/api/gitlab-ci/project")
	var result string
	resp, err := safebug.request().
		SetBody(body).
		SetResult(&result).
		Post(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() != 200 {
		logrus.Error(resp.StatusCode())
		logrus.Error(resp.Result())
		logrus.Panic(resp.Error())
	}
	return result
}

func (safebug *SafeBug) GetSastIssues(projectGitlabId string) []SastIssueSummary {
	api, _ := url.JoinPath(safebug.BaseURL, fmt.Sprintf("/api/gitlab-ci/project/%s/issue", projectGitlabId))
	request := safebug.request()
	var result []SastIssueSummary
	resp, err := request.
		SetResult(&result).
		Get(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() != 200 {
		logrus.Error(resp.StatusCode())
		logrus.Error(resp.Result())
		logrus.Panic(resp.Error())
	}
	return result
}

func (safebug *SafeBug) CreateSastIssue(projectGitlabId string, body CreateSastIssueRequest) string {
	api, _ := url.JoinPath(safebug.BaseURL, fmt.Sprintf("/api/gitlab-ci/project/%s/issue", projectGitlabId))
	var result string
	resp, err := safebug.request().
		SetBody(body).
		SetResult(&result).
		Post(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() != 200 {
		logrus.Error(resp.StatusCode())
		logrus.Error(resp.Result())
		logrus.Panic(resp.Error())
	}
	return result
}

func (safebug *SafeBug) CompleteGitlabSast(projectGitlabId string, body SastReportSummary) {
	api, _ := url.JoinPath(safebug.BaseURL, fmt.Sprintf("/api/gitlab-ci/project/%s/sast/completed", projectGitlabId))
	resp, err := safebug.request().
		SetBody(body).
		Post(api)
	if err != nil {
		logrus.Error(resp.StatusCode())
		logrus.Error(resp.Result())
		logrus.Panic(resp.Error())
	}
	if resp.StatusCode() != 200 {
		logrus.Panic(resp.Error())
	}
}

func (safebug *SafeBug) CommentOnIssue(issueId string, body CommentIssue) {
	api, _ := url.JoinPath(safebug.BaseURL, "api/issue/"+issueId+"/comment")
	resp, err := safebug.request().
		SetBody(body).
		Post(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	if resp.StatusCode() != 200 {
		logrus.Error(resp.StatusCode())
		logrus.Error(resp.Result())
		logrus.Panic(resp.Error())
	}
}

func (safebug *SafeBug) GetSastThresholdConfig(projectGitlabId string) SastThresholdConfig {
	api, _ := url.JoinPath(safebug.BaseURL, fmt.Sprintf("/api/gitlab-ci/project/%s/sast/threshold", projectGitlabId))
	var result SastThresholdConfig
	_, err := safebug.request().
		SetResult(&result).
		Get(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	return result
}

func (safebug *SafeBug) GetBaseURL() string {
	api, _ := url.JoinPath(safebug.BaseURL, "/api/config/base-url")
	var baseUrl string
	_, err := safebug.request().
		SetResult(&baseUrl).
		Get(api)
	if err != nil {
		logrus.Panic(err.Error())
	}
	return baseUrl
}

func (safebug *SafeBug) request() *resty.Request {
	return safebug.client.R().SetHeader("Authorization", "Bearer "+safebug.AccessToken)
}
