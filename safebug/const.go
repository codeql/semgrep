package safebug

const (
	Open          = "Open"
	Retesting     = "Retesting"
	Verified      = "Verified"
	Resolved      = "Resolved"
	WontFix       = "WontFix"
	Duplicated    = "Duplicated"
	NotApplicable = "NotApplicable"
	Spam          = "Spam"
	Ignore        = "Ignore"
	//severity
	Critical = "Critical"
	High     = "High"
	Medium   = "Medium"
	Low      = "Low"
	Info     = "Info"
)
