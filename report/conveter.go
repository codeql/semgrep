package report

import (
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"semgrep/types"
)

func ConvertJsonToReport(data []byte) (Report, error) {
	var report Report
	err := json.Unmarshal(data, &report)
	return report, err
}

func ConvertReportToVulnerabilities(report Report, rootPath string) []types.Vulnerability {
	var vulnerabilities []types.Vulnerability
	for _, result := range report.Results {
		name := fmt.Sprintf("Found vulnerability at %s:%d", result.Path, result.Start.Line)
		category := "Other"
		if len(result.Extra.Metadata.VulnerabilityClass) > 0 {
			name = fmt.Sprintf("%s at %s:%d", result.Extra.Metadata.VulnerabilityClass[0], result.Path, result.Start.Line)
			category = result.Extra.Metadata.VulnerabilityClass[0]
		}
		description := result.Extra.Message
		//dataflow
		if result.Extra.Dataflow != nil {
			var taintFlows []*TaintLocation
			taintSource := ConvertCliLoc(result.Extra.Dataflow.TaintSource)
			if taintSource != nil {
				taintFlows = append(taintFlows, taintSource)
			}
			taintSinks := ConvertCliCall(result.Extra.Dataflow.TaintSink)
			if len(taintSinks) == 0 {
				taintSink := ConvertCliLoc(result.Extra.Dataflow.TaintSink)
				if taintSink != nil {
					taintSinks = append(taintSinks, taintSink)
				}
			}
			taintFlows = append(taintFlows, taintSinks...)
			if len(taintFlows) > 0 {
				description += "\n\n**Data Flow**\n"
				for _, taint := range taintFlows {
					uri := fmt.Sprintf("%s/-/blob/%s/%s#L%d-L%d", rootPath, os.Getenv("CI_COMMIT_SHA"), taint.Location.Path, taint.Location.Start.Line, taint.Location.End.Line)
					description += fmt.Sprintf("- [`%s` @ %s:%d](%s)\n", taint.Content, taint.Location.Path, taint.Location.Start.Line, uri)
				}
			}
		}
		//cwe
		if len(result.Extra.Metadata.Cwe) > 0 {
			description += "\n\n**CWE**\n"
			for _, cwe := range result.Extra.Metadata.Cwe {
				description += fmt.Sprintf("- %s\n", cwe)
			}
		}
		//owasp
		if len(result.Extra.Metadata.Owasp) > 0 {
			description += "\n\n**OWASP**\n"
			for _, s := range result.Extra.Metadata.Owasp {
				description += fmt.Sprintf("- %s\n", s)
			}
		}
		//reference
		if len(result.Extra.Metadata.References) > 0 {
			description += "\n\n**References**\n"
			for _, s := range result.Extra.Metadata.References {
				description += fmt.Sprintf("- %s\n", s)
			}
		}
		//source
		description += "\n\n**Source**\n"
		description += fmt.Sprintf("- %s\n", result.Extra.Metadata.Source)
		location := fmt.Sprintf("%s/-/blob/%s/%s#L%d-L%d", rootPath, os.Getenv("CI_COMMIT_SHA"), result.Path, result.Start.Line, result.End.Line)
		var vulnerability = types.Vulnerability{
			ID:          "",
			Name:        name,
			Description: description,
			Category:    category,
			Severity:    result.Severity(),
			Location:    location,
			Identity:    result.Extra.Fingerprint,
		}
		vulnerabilities = append(vulnerabilities, vulnerability)
	}
	return vulnerabilities
}

func ConvertCliLoc(node []interface{}) *TaintLocation {
	if len(node) == 2 && reflect.TypeOf(node[0]).Kind() == reflect.String && node[0].(string) == "CliLoc" && reflect.TypeOf(node[1]).Kind() == reflect.Slice {
		locNode := node[1].([]interface{})
		return convertLocNode(locNode)
	}
	return nil
}

func ConvertCliCall(node []interface{}) []*TaintLocation {
	var result []*TaintLocation
	if len(node) == 2 && reflect.TypeOf(node[0]).Kind() == reflect.String && node[0].(string) == "CliCall" && reflect.TypeOf(node[1]).Kind() == reflect.Slice {
		callNode := node[1].([]interface{})
		if len(callNode) == 3 {
			if reflect.TypeOf(callNode[0]).Kind() == reflect.Slice {
				taint := convertLocNode(callNode[0].([]interface{}))
				if taint != nil {
					result = append(result, taint)
				}
			}
			if reflect.TypeOf(callNode[1]).Kind() == reflect.Slice {
				for _, taintNode := range callNode[1].([]interface{}) {
					data, _ := json.Marshal(taintNode)
					var taint TaintLocation
					err := json.Unmarshal(data, &taint)
					if err == nil && taint.Location.Path != "" {
						result = append(result, &taint)
					}
				}
			}
			if reflect.TypeOf(callNode[2]).Kind() == reflect.Slice {
				taint := ConvertCliLoc(callNode[2].([]interface{}))
				if taint != nil {
					result = append(result, taint)
				}
			}
		}
	}
	return result
}

func convertLocNode(node []interface{}) *TaintLocation {
	if len(node) == 2 && reflect.TypeOf(node[1]).Kind() == reflect.String {
		data, _ := json.Marshal(node[0])
		var location Location
		err := json.Unmarshal(data, &location)
		if err == nil && location.Path != "" {
			return &TaintLocation{
				Location: location,
				Content:  node[1].(string),
			}
		}
	}
	return nil
}
