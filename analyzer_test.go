package main

import (
	"encoding/json"
	"io/ioutil"
	"semgrep/report"
	"semgrep/safebug"
	"testing"
)

const Token = "f5e334f3-797f-4473-8d95-e5b71d1341f4-fb0b41db-782e-4225-a4ba-f681e0bc2ef2"
const SafeBugUrl = "http://localhost:8000"

func TestAnalyze(t *testing.T) {
	analyzer := Analyzer{
		safebug: safebug.NewSafeBug(SafeBugUrl, Token),
		GitlabCI: EnvGitlabCI{
			ProjectID:     "50471840",
			ProjectURL:    "https://gitlab.com/test/example",
			ProjectPath:   "test/example",
			DefaultBranch: "main",
			CommitBranch:  "main",
			PipelineID:    "1",
			PipelineURL:   "https://gitlab.com/test/example/-/pipelines/1",
		},
	}

	jsonBytes, err := ioutil.ReadFile("testdata/semgrep1.40.json")
	if err != nil {
		t.Failed()
	}
	var rep report.Report
	err = json.Unmarshal(jsonBytes, &rep)
	if err != nil {
		t.Failed()
	}
	if err != nil {
		logger.Error(err.Error())
	}
	vulns := report.ConvertReportToVulnerabilities(rep, analyzer.GitlabCI.ProjectURL)
	vulns = vulns[:10]
	analyzer.Analyze(vulns)
}
