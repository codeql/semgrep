package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"strings"
)

type Formatter struct {
	logrus.TextFormatter
}

func (f *Formatter) Format(entry *logrus.Entry) ([]byte, error) {
	// this whole mess of dealing with ansi color codes is required if you want the colored output otherwise you will lose colors in the log levels
	var levelColor int
	switch entry.Level {
	case logrus.DebugLevel, logrus.TraceLevel:
		levelColor = 31 // gray
	case logrus.WarnLevel:
		levelColor = 33 // yellow
	case logrus.ErrorLevel, logrus.FatalLevel, logrus.PanicLevel:
		levelColor = 31 // red
	default:
		levelColor = 36 // blue
	}
	return []byte(fmt.Sprintf("\x1b[%dm%s\x1b[0m - %s\n", levelColor, strings.ToUpper(entry.Level.String()), entry.Message)), nil
}

var logger = logrus.Logger{
	Out:       os.Stdout,
	Level:     logrus.InfoLevel,
	Formatter: &Formatter{},
}
