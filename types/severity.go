package types

type Severity struct {
	Critical int
	High     int
	Medium   int
	Low      int
}
