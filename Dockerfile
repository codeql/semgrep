# When updating version make sure to check on semgrepignore file as well
FROM golang:1.20-alpine AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/buildapp
COPY . .
# variable to the most recent version from the CHANGELOG.md file
RUN PATH_TO_MODULE=`go list -m` && go build -o /analyzer

FROM returntocorp/semgrep:1.40
# add curl
RUN apk update
RUN apk add curl zip
ENV SEMGREP_R2C_INTERNAL_EXPLICIT_SEMGREPIGNORE "/semgrepignore"
ENV PIP_NO_CACHE_DIR=off
COPY semgrepignore /semgrepignore
COPY semgrep-core-proprietary.zip semgrep-core-proprietary.zip
RUN unzip semgrep-core-proprietary.zip -d /usr/local/bin/
RUN chmod +x /usr/local/bin/semgrep-core-proprietary
COPY --from=build /analyzer /analyzer
COPY rules /rules
ENTRYPOINT []
CMD ["/analyzer", "run"]
