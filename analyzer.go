package main

import (
	"bufio"
	"fmt"
	"github.com/fatih/color"
	"github.com/rodaine/table"
	"io"
	"os"
	"os/exec"
	"semgrep/safebug"
	"semgrep/types"
	"strings"
)

type EnvGitlabCI struct {
	ProjectID     string
	ProjectURL    string
	ProjectPath   string
	DefaultBranch string
	CommitBranch  string
	PipelineID    string
	PipelineURL   string
	CommitSha     string
}

func NewEnvGitlabCI() EnvGitlabCI {
	return EnvGitlabCI{
		ProjectID:     os.Getenv("CI_PROJECT_ID"),
		ProjectURL:    os.Getenv("CI_PROJECT_URL"),
		ProjectPath:   os.Getenv("CI_PROJECT_PATH"),
		DefaultBranch: os.Getenv("CI_DEFAULT_BRANCH"),
		CommitBranch:  os.Getenv("CI_COMMIT_BRANCH"),
		PipelineID:    os.Getenv("CI_PIPELINE_ID"),
		PipelineURL:   os.Getenv("CI_PIPELINE_URL"),
		CommitSha:     os.Getenv("CI_COMMIT_SHA"),
	}
}

type Analyzer struct {
	Configs       string
	Severities    string
	ProEngine     bool
	ExcludedPaths string
	Verbose       bool
	GitlabCI      EnvGitlabCI
	SarifOutput   bool
	safebug       *safebug.SafeBug
}

func (analyzer *Analyzer) Scan(projectPath string) ([]byte, error) {
	outputPath := "scan_results.json"
	args := analyzer.args(projectPath, outputPath)
	cmd := exec.Command("semgrep", args...)
	logger.Info(cmd.String())
	cmd.Env = os.Environ()
	stdout, _ := cmd.StdoutPipe()
	err := cmd.Start()
	if err != nil {
		return nil, err
	}
	go printStdout(stdout)
	err = cmd.Wait()
	if err != nil {
		return nil, err
	}
	reader, _ := os.Open(outputPath)
	return io.ReadAll(reader)
}

func (analyzer *Analyzer) Analyze(vulnerabilities []types.Vulnerability) {
	//
	analyzer.safebug.RefreshToken()
	// create or update project gitlab
	projectId := analyzer.safebug.CreateOrUpdateProjectGitlab(safebug.CreateUpdateProjectGitlabRequest{
		Name:             analyzer.GitlabCI.ProjectPath,
		ProjectGitlabUrl: analyzer.GitlabCI.ProjectURL,
		ProjectGitlabId:  analyzer.GitlabCI.ProjectID,
	})
	mSeverity := make(map[string]int)
	isDefaultBranch := analyzer.GitlabCI.DefaultBranch == analyzer.GitlabCI.CommitBranch
	// make map issues
	mIssues := make(map[string]safebug.SastIssueSummary)
	for _, issue := range analyzer.safebug.GetSastIssues(analyzer.GitlabCI.ProjectID) {
		mIssues[issue.Identity] = issue
	}
	var newIssues []safebug.SastIssueSummary
	openIssue := 0
	verifiedIssue := 0
	ignoreIssue := 0
	for _, vulnerability := range vulnerabilities {
		if issue, exists := mIssues[vulnerability.Identity]; exists == false {
			// analyzer is new vulnerability
			issueId := analyzer.safebug.CreateSastIssue(analyzer.GitlabCI.ProjectID, safebug.CreateSastIssueRequest{
				Name:        vulnerability.Name,
				Category:    vulnerability.Category,
				Description: vulnerability.Description,
				Severity:    vulnerability.Severity,
				Solution:    vulnerability.Solution,
				Reproduce:   vulnerability.Reproduce,
				Endpoint:    vulnerability.Location,
				Identity:    vulnerability.Identity,
			})
			mSeverity[vulnerability.Severity] = mSeverity[vulnerability.Severity] + 1
			newIssues = append(newIssues, safebug.SastIssueSummary{
				ID:       issueId,
				Name:     vulnerability.Name,
				Identity: vulnerability.Identity,
				Severity: vulnerability.Severity,
				Status:   safebug.Open,
			})
		} else {
			// the vulnerability is false positive
			if issue.Status == safebug.NotApplicable || issue.Status == safebug.WontFix || issue.Status == safebug.Spam || issue.Status == safebug.Duplicated || issue.Status == safebug.Ignore {
				ignoreIssue += 1
			} else {
				if issue.Status == safebug.Open {
					openIssue += 1
				}
				if issue.Status == safebug.Verified {
					verifiedIssue += 1
					if isDefaultBranch {
						mSeverity[issue.Severity] += 1
					}
				}
			}
			delete(mIssues, issue.Identity)
		}
	}
	baseUrl := analyzer.safebug.GetBaseURL()
	analyzer.safebug.RefreshToken()
	// new issue
	if len(newIssues) > 0 {
		logger.Info(fmt.Sprintf("Found %d new vulnerabilities", len(newIssues)))
		tbl := table.New("ID", "Name", "Severity", "URL")
		tbl.WithHeaderFormatter(color.New(color.BgCyan, color.Underline).SprintfFunc()).
			WithFirstColumnFormatter(color.New(color.FgYellow).SprintfFunc())
		for index, issue := range newIssues {
			tbl.AddRow(index+1, issue.Name, issue.Severity, baseUrl+"/#/issue/"+issue.ID)
		}
		tbl.Print()
	} else {
		logger.Info("There are no new vulnerabilities found")
	}
	// resolved issue
	resolved := safebug.Resolved
	retesting := safebug.Retesting
	resolvedCount := 0
	for fingerprint, issue := range mIssues {
		if issue.Status == safebug.Resolved {
			continue
		}
		if isDefaultBranch {
			analyzer.safebug.CommentOnIssue(issue.ID, safebug.CommentIssue{
				Status: &resolved,
			})
			resolvedCount += 1
		} else {
			if issue.Status != safebug.Retesting && issue.Status != safebug.Resolved {
				comment := fmt.Sprintf("This vulnerability has been fixed on branch **%s**. Merge to **%s** branch to resolved analyzer vulnerability",
					analyzer.GitlabCI.CommitBranch, analyzer.GitlabCI.DefaultBranch)
				analyzer.safebug.CommentOnIssue(issue.ID, safebug.CommentIssue{
					Comment: &comment,
					Status:  &retesting,
				})
				resolvedCount += 1
			} else {
				delete(mIssues, fingerprint)
			}
		}
	}
	if resolvedCount > 0 {
		logger.Info(fmt.Sprintf("Fixed %d vulnerabilities on %s branch", resolvedCount, analyzer.GitlabCI.CommitBranch))
		tbl := table.New("ID", "Name", "Severity", "URL")
		tbl.WithHeaderFormatter(color.New(color.BgCyan, color.Underline).SprintfFunc()).
			WithFirstColumnFormatter(color.New(color.FgYellow).SprintfFunc())
		index := 1
		for _, issue := range mIssues {
			if issue.Status != safebug.Resolved {
				tbl.AddRow(index, issue.Name, issue.Severity, baseUrl+"/#/issue/"+issue.ID)
				index += 1
			}
		}
		tbl.Print()
	} else {
		logger.Info("There are no vulnerabilities resolved")
	}
	// quality gate
	sastConfig := analyzer.safebug.GetSastThresholdConfig(analyzer.GitlabCI.ProjectID)
	if sastConfig.Enable {
		logger.Info("Quality Gate Mode: Prevent")
		passQualityGate := qualityGate(sastConfig, mSeverity)
		analyzer.safebug.CompleteGitlabSast(analyzer.GitlabCI.ProjectID, safebug.SastReportSummary{
			Branch:      analyzer.GitlabCI.CommitBranch,
			PipelineId:  analyzer.GitlabCI.PipelineID,
			PipelineUrl: analyzer.GitlabCI.PipelineURL,
			QualityGate: passQualityGate,
			NewIssue:    len(newIssues),
			FixedIssue:  resolvedCount,
		})
		if passQualityGate {
			logger.Info("Quality Gate: Success")
		} else {
			logger.Error("Quality Gate: Failed")
			if isDefaultBranch {
				logger.Infof("Quality Gate Failed because there were still many unfixed vulnerabilities on the %s branch", analyzer.GitlabCI.DefaultBranch)
			}
			logger.Info("Details here:", baseUrl+"/#/project/"+projectId)
			os.Exit(1)
		}
	} else {
		logger.Warning("Quality Gate Mode: Monitor")
	}

}

func (analyzer *Analyzer) args(projectPath, outputPath string) []string {
	args := []string{
		"scan",
		"--no-rewrite-rule-ids",
		"--dataflow-traces",
		"--disable-version-check",
		"--no-git-ignore",
		"-o", outputPath,
	}
	if analyzer.SarifOutput {
		args = append(args, "--sarif")
	} else {
		args = append(args, "--json")
	}
	if strings.TrimSpace(analyzer.ExcludedPaths) != "" {
		excludes := strings.Split(analyzer.ExcludedPaths, ",")
		for _, exclude := range excludes {
			args = append(args, "--exclude", strings.TrimSpace(exclude))
		}
	}
	if strings.TrimSpace(analyzer.ExcludedPaths) != "" {
		severities := strings.Split(analyzer.Severities, ",")
		for _, severity := range severities {
			if severity == "INFO" || severity == "WARNING" || severity == "ERROR" {
				args = append(args, "--severity", strings.TrimSpace(severity))
			}
		}
	}

	if analyzer.ProEngine {
		args = append(args, "--pro")
	}

	if strings.TrimSpace(analyzer.Configs) != "" {
		configs := strings.Split(analyzer.Configs, ",")
		for _, config := range configs {
			args = append(args, "--config", strings.TrimSpace(config))
		}
	}
	if analyzer.Verbose {
		args = append(args, "--verbose")
	}
	args = append(args, projectPath)
	return args
}

func qualityGate(projectGitlab safebug.SastThresholdConfig, mSeverity map[string]int) bool {
	logger.Info("Severity New Vulnerabilities")
	tbl := table.New("CRITICAL", "HIGH", "MEDIUM", "LOW", "INFO")
	tbl.WithHeaderFormatter(color.New(color.BgCyan, color.Underline).SprintfFunc())
	tbl.AddRow(mSeverity[safebug.Critical], mSeverity[safebug.High], mSeverity[safebug.Medium], mSeverity[safebug.Low], mSeverity[safebug.Info])
	tbl.Print()
	fmt.Println()
	logger.Info("Quality Gate Config")
	tbl = table.New("CRITICAL", "HIGH", "MEDIUM", "LOW")
	tbl.WithHeaderFormatter(color.New(color.BgCyan, color.Underline).SprintfFunc())
	tbl.AddRow(projectGitlab.Critical, projectGitlab.High, projectGitlab.Medium, projectGitlab.Low)
	tbl.Print()

	if projectGitlab.Critical > 0 && mSeverity[safebug.Critical] >= projectGitlab.Critical {
		return false
	}
	if projectGitlab.High > 0 && mSeverity[safebug.High] >= projectGitlab.High {
		return false
	}
	if projectGitlab.Medium > 0 && mSeverity[safebug.Medium] >= projectGitlab.Medium {
		return false
	}
	if projectGitlab.Low > 0 && mSeverity[safebug.Low] >= projectGitlab.Low {
		return false
	}
	return true
}

func printStdout(stdout io.ReadCloser) {
	reader := bufio.NewReader(stdout)
	line, _, err := reader.ReadLine()
	for {
		if err != nil || line == nil {
			break
		}
		logger.Println(string(line))
		line, _, err = reader.ReadLine()
	}
}
